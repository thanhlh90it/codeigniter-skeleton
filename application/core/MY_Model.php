<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	protected $ci;
	
	public function __construct(){
		parent::__construct();
		$this->ci =& get_instance();
		// uncomment after config database connection
		// $this->ci->load->database();
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */